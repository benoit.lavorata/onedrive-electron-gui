# onedrive-electron-gui

This is an Electron app which embed a Node-RED dashboard monitoring/acting on onedrive from skilion.
Will only work for linux, tested on ubuntu 19.04.

Stage: development.
Based on:
- https://github.com/dceejay/electron-node-red
- https://github.com/skilion/onedrive


## Requirements
- skilion/onedrive installed and configured (https://github.com/skilion/onedrive)
- set onedrive as service using ```systemctl --user enable onedrive```
- install nethogs for measuring bandwith ```sudo apt-get install nethogs -y```
- set capabilities on nethogs to run as non-root ```sudo setcap "cap_net_admin,cap_net_raw=ep" /usr/sbin/nethogs```
- To build: nodejs, npm


## Preview
![Dashboard](onedrive-gui-test.png)

The node-red flow is in the file electronflow.json, illustrated below:
![Backend](node-red.png)


## What it does
UI shows bandwith up/down, logs, latest action, START and STOP buttons

It uses these commands:
```
systemctl --user start onedrive
systemctl --user stop onedrive
journalctl --user-unit onedrive -f
nethogs -t
```


## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://gitlab.com/benoit.lavorata/onedrive-electron-gui.git
# Go into the repository
cd onedrive-electron-gui
# Install dependencies and run the app
npm install && npm run clean && npm start
```

## TL:DR - building runtimes

Run `npm run pack` to create packages for all platforms - these are the files required to run, they are not binary installers.

Builds are created in the `build` directory. Runtimes are created in the `../electron-bin` directory.


## Packaging your application

If you want to distribute executables of this project, the easiest way is to use electron-packager:

```
sudo npm install -g electron-packager

# build for Linux 64 bits
electron-packager . Node-RED --icon=nodered.icns --platform=linux --arch=x64 --out=build --overwrite
```

### To package as a deb

`npm run build:linux64` or `npm run build:linux32` - for Intel Linux

Look at `https://github.com/jordansissel/fpm`

    fpm -s dir -t deb -f -n node-red-electron -v 0.16.2 -m your-email@example.com -a i386 Node-RED-linux-ia32/
    fpm -s dir -t deb -f -n node-red-electron -v 0.16.2 -m your-email@example.com -a x86_64 Node-RED-linux-x64/

Use **sudo dpkg -i ...*** to install the correct deb for your architecture.

Use `Node-RED` command to run. Flows are stored in `~/.node-red`.


## License [CC0 (Public Domain)](LICENSE.md)

## See also
 - **Stand-alone Starter Project** - https://github.com/dceejay/node-red-project-starter
 - **Bluemix Starter Project** - https://github.com/dceejay/node-red-bluemix-starter
